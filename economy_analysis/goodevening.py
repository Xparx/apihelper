import datetime as _datetime
import urllib.request as _request
import json as _json
import pandas as _pd
from bs4 import BeautifulSoup as _BeautifulSoup


class historicalPrice(object):
    """Documentation for historicalPrice

    """
    def __init__(self):

        self._baseurl = "http://globalquote.morningstar.com/globalcomponent/RealtimeHistoricalStockData.ashx?"

    def dowload(self, ticker, fr="1900-1-1", to=_datetime.datetime.today().strftime('%Y-%m-%d'), currency="USD"):

        t = "ticker={t}"
        sw = "showVol=true"
        dt = "dtype=his"
        c = "curry={currency}"
        rang = "range={fr}|{to}"

        url = ("&".join([self._baseurl, t, sw, dt, "f=d", c, rang, "isD=true", "isS=true", "hasF=true", "ProdCode=DIRECT"])).format(t=ticker, currency=currency, fr=fr, to=to)
        with _request.urlopen(url) as response:
            url_string = response.read().decode()

        self.data = _json.loads(url_string)

        output = {}
        output["price"] = self._priceparse()
        output["dividend"] = self._dividendparse()

        return output

    def _priceparse(self):

        price = _pd.DataFrame(self.data['PriceDataList'][0]['Datapoints'])
        dates = self._dateconvert(self.data['PriceDataList'][0]["DateIndexs"])
        price.index = _pd.PeriodIndex(dates, freq="D")
        price.columns = ["Open", "High", "Low", "Close"]

        return price

    def _dateconvert(self, date_list):

        return [_datetime.date(1900, 1, 1) + _datetime.timedelta(int(d)) for d in date_list]

    def _dividendparse(self):

        parsed_dividend = _pd.DataFrame()
        for i in self.data["DividendData"]:
            date = _datetime.date(*(int(i) for i in i["Date"].split('-')))
            optype = i["Type"]
            v = i["Desc"].replace(optype + ":", "").replace(optype + "s:", "").replace("<br>", "")
            if optype != "Splits":
                v = float(v)

            parsed_dividend.loc[_pd.to_datetime(date), optype] = v

            # parsed_dividend = parsed_dividend.to_period()

        return parsed_dividend


class morningstarLookup():
    """Documentation for morningstarLookup

    """
    def __init__(self):

        self._baseurl = "http://quote.morningstar.com/TickerLookupResult.html?"
        # self._baseurl = "http://quote.morningstar.com/TickerLookupResult.html?rbtnTicker=NameContain&ticker=teknik&x=0&y=0&SC=&pageno=0&TLC=M"

        # ticker=F

    def search(self, search, should="contain"):

        if should.lower() in "contain":
            search_opt = "rbtnTicker=NameContain&"
        elif should.lower() == "wtartswith":
            search_opt = "rbtnToicker=NameStart&"
        else:
            raise ValueError('Should argument can only be "Contain" or "Startswith"')

        t = "ticker={}".format(search)
        tail = "&pageno=0&view=All&TLC=M"

        url = "".join([self._baseurl, search_opt, t, tail])

        results = self._load_url(url)

        results = self._parse_soup(results)

        return results

    def _load_url(self, url):

        with _request.urlopen(url) as response:
            html_doc = response.read().decode()

        soup = _BeautifulSoup(html_doc, 'html.parser')

        return soup

    def _parse_soup(self, soup):

        columns = ["morningstar ticker", "ticker", "Exchange", "name"]
        tickers_search = _pd.DataFrame(columns=columns)
        for table_row in soup.find(attrs={'name': 'Result'}).select("table")[1].select("tr"):
            # cells = table_row.findAll('td')
            c = (i for i in columns)
            cells = table_row.find_all(attrs={"class": "L2"})
            for tc in cells:

                header = next(c)
                if header == "morningstar ticker":
                    name = tc.text.strip()

                value = tc.text.strip()
                if tc.find("a"):
                    value = tc.find("a")["href"]
                    value = value.split("?")[-1].replace("ticker=", "")

                tickers_search.loc[name, header] = value

        return tickers_search
