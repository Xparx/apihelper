from setuptools import setup

setup(name='apihelper',
      version='0.1',
      description='Helpers for different data APIs.',
      url='',
      author='Andreas Tjärnberg',
      author_email='andreas.tjarnberg@fripost.org',
      license='',
      packages=['economy_analysis'],
      install_requires=[
          'numpy',
          'pandas',
      ],
      zip_safe=False)
